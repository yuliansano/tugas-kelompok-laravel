<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kritik extends Model
{
    //
    protected $table = 'kritik';

    protected $fillable = ['users_id', 'film_id', 'isi', 'point'];

    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }

    public function film()
    {
        return $this->belongsTo('App\Film');
    }
}
