<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Register</title>
  </head>

  <body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
      @csrf
      <label>First Name</label> <br /> <br>
      <input type="text" name="Nama" /> <br /> <br>
      <label>Last Name</label> <br /> <br>
      <input type="text" name="Nama1" /> <br /> <br>
      <label>Gander</label> <br /> <br>
        <input type="radio" name="Gander">Male <br>
        <input type="radio" name="Gander">Female <br>
        <input type="radio" name="Gander">Other <br> <br>
        <label>Nationality</label> <br> <br>
        <select name="Nationality">
            <option value="1">Indonesia</option>
            <option value="2">Australia</option>
            <option value="3">Amerika</option>
        </select> <br> <br>
        <label>Language Spoken</label> <br> <br>
        <input type="checkbox" name="Language"> Bahasa Indonesia <br>
        <input type="checkbox" name="Language"> English <br>
        <input type="checkbox" name="Language"> Other <br> <br>
        <label>Bio</label> <br> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br> <br>

        <input type="submit" value="Sign Up">
    </input>
    </form>
  </body>
</html>
