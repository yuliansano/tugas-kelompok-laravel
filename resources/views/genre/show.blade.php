@extends('layout.master')

@section('judul')
Halaman Detail {{$genre->nama}}    
@endsection

@section('judul1')
Detail Film {{$genre->nama}}    
@endsection

@section("content")

    <h3> {{$genre ->nama}} </h3>

    <div class="row">
        @foreach ($genre->film as $item)
            <div class="col-4">
                <div class="card" >
                    <img class="card-img-top" src="{{asset('gambar/'.$item->poster)}}" alt="Card image cap">
                    <div class="card-body">
                    <h3> {{$item->judul}}</h3>
                    <h5> Produksi th : {{$item->tahun}}<br></h5>
                    <p class="card-text"> {{$item->ringkasan}}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    

  

@endsection