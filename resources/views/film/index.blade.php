@extends('layout.master')

@section('judul')
Halaman Film    
@endsection

@section('judul1')
Daftar Film 
@endsection

@section("content")
    @auth
     <a href ='/film/create' class='btn btn btn-dark btn-sm mb-3'> Tambah data </a> 
    @endauth
   <div class='row'>
        @forelse ($film as $item)
            
        
        <div class = 'col-4'>
            <div class="card" >
                <img class="card-img-top" src="{{asset('gambar/'.$item->poster)}}" alt="Card image cap">
                <div class="card-body">
                  <span class="badge badge-info">{{$item->genre->nama}}</span>
                  <h3> {{$item->judul}}</h3>
                  <h5> Produksi th : {{$item->tahun}}<br></h5>
                  <p class="card-text"> {{Str::limit($item->ringkasan,150)}}</p>
                  @auth
                    <form action="/film/{{$item->id}}" method = "POST">
                        @method('Delete')
                        @csrf
                        <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/film/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                        <input type ="submit" class="btn btn-danger btn-sm" value = 'Delete'> 
                    </form>
                  @endauth
                  @guest
                        <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                  @endguest
                </div>
            </div>


        </div>
        @empty
            <p> DAFTAR FILM MASIH KOSONG </P>
        @endforelse
    </div>
@endsection