@extends('layout.master')

@section('judul')
Halaman Edit Film {{$film->judul}}    
@endsection

@section('judul1')
Halaman Edit Film {{$film->judul}}   
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/ep3gqe914t18dwiubahl0o3uo6cn4kq3bz7b30f84zhjb6qm/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

@section("content")
    <form action ="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label >Judul</label>
            <input type="string" value={{$film->judul}} name ="judul" class="form-control">
        </div>
        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Ringkasan</label>
            <textarea name = "ringkasan" class="form-control"> {{$film->ringkasan}}</textarea>
        </div>
        @error('ringkasan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>tahun</label>
            <input type="integer" value={{$film->tahun}} name ="tahun" class="form-control">
        </div>
        @error('tahun')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <div class="form-group">
            <label>Genre</label>
            <select name ='genre_id' class="form-control" id ="">
                <option value ="">-- pilih genre --</option>
                @foreach ($genre as $item)
                    @if ($item->id === $film-> genre_id) 
                        <option value={{$item->id}} selected>{{$item -> nama}}</option>
                                          
                    @else
                        
                        <option value={{$item->id}} >{{$item -> nama}}</option>
                    @endif
                @endforeach  
            </select>
        </div>
        
        @error('genre_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Poster</label>
            <input type="file" name ="poster" class="form-control">
        </div>
        @error('poster')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection