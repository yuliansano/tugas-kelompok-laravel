@extends('layout.master')

@section('judul')
Halaman Detail {{$film->judul}}    
@endsection

@section('judul1')
Detail Film {{$film->judul}}    
@endsection

@section("content")
    <img src="{{asset('gambar/'.$film->poster)}}" alt="poster tidak ada">
    <h3> Produksi tahun : {{$film -> tahun}} <br> </h3>
    <h4> Sinopsis : <br> </h4>
    <p> {{$film -> ringkasan}} </p>

    <h1>Kritik</h1>
    @guest
        <small><b>Silahkan Login Untuk Menambah Komentar!!!!</b></small>
    @endguest

    @foreach ($film->kritik as $item)
        <div class="card">
            <div class="card-body">
             <small><b>{{$item->user->name}}</b> <br></small>
             <small><b>Rating : {{$item->point}} /10</b></small>
             <p class="card-text">{{$item->isi}}</p>  
            </div>
        </div>
    @endforeach   

    @empty($item)
    <div class="card">
        <div class="card-body">
                <p>Belum Ada Komentar!!!!</p>
        </div>
    </div>
    @endempty
    

    @auth
    <form action ="/kritik" method="POST" enctype="multipart/form-data" class="my-3">
        @csrf
        
        <div class="form-group">
            <label>Komentar Anda</label>
            <input type="hidden" name="film_id" value="{{$film->id}}">
            <textarea name = "isi" class="form-control" cols="30" rows = "5"> </textarea>
        </div>
        @error('isi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Point</label>
            <input type="number" name ="point" class="form-control">
        </div>
        @error('point')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    @push('script')
    <script src="https://cdn.tiny.cloud/1/ep3gqe914t18dwiubahl0o3uo6cn4kq3bz7b30f84zhjb6qm/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
          selector: 'textarea',
          plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
          toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
          toolbar_mode: 'floating',
          tinycomments_mode: 'embedded',
          tinycomments_author: 'Author name',
        });
      </script>
    @endpush

    @endauth
    
    
    <a href ='/film' class ='bnt btn-primary'> kembali </a>
@endsection